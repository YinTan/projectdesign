
function content_done(s){
    var art_content = $('#allarticle');
    art_content.html(s.content); 
}

function content_fail(xhr, status){
    var art_content = $('#allarticle');
    art_content.html('<p style="color: red"><strong>加载文章出错</strong></p>')
}
function ajaxLog(s) {
    console.log(s);
}

function post_done(s){
    var post_list = $('#epostlist');
    var old_post = $('[id^=post]');
    old_post.remove();
    //post_list.remove();
    if(s.length <= 0){
        post_list.append("<dt id=\"postnone\"><strong style=\"color:red;\">暂无数据！</strong></dt>")
    }else{
        for(var i=0; i < s.length; i++){
            if(s[i].title){
                if(s[i].sentiment === "P"){
                    post_list.append(`<dt id ="post${s[i].id}">${s[i].title}<img src="static/rss/happy.png"  alt="积极" style="heihgt:8%;width:8%;float:right;"</dt>`);
                }else if(s[i].sentiment === "N"){
                    post_list.append(`<dt id ="post${s[i].id}">${s[i].title}<img src="static/rss/sad.png"  alt="消极" style="heihgt:8%;width:8%;float:right;"</dt>`);
                }else if(s[i].sentiment === "M"){
                    post_list.append(`<dt id ="post${s[i].id}">${s[i].title}<img src="static/rss/zhongli.gif"  alt="中立" style="heihgt:8%;width:8%;float:right;"</dt>`);
                }else{
                    post_list.append(`<dt id ="post${s[i].id}">${s[i].title}</dt>`);
                }
            }            
        }
        $(function () {
            var post = $('[id^=post]');
            post.click(function () {
                var id = /^post([0-9]+)/.exec($(this).attr('id'))[1];
                $.ajax(`/epost/${id}/`, {
                    method: "POST",
                    dataType: 'json'
                }).done(function (data) {
                    content_done(data);
                }).fail(function (xhr, status) {
                    content_fail(xhr, status);
                }).always(function () {
                    //ajaxLog('请求完成: 无论成功或失败都会调用');
                })
            });
        });        
    }
}



function post_fail(xhr, status){
    var post_list = $('#postlist');
    post_list.html("<p style=\"color:red;\" >请求数据失败</p>");
}

$(function () {
    var feed = $('[id^=feed]');
    feed.click(function () {
        $(this).find("span").text("");
        showdiv('request post data...');
        var id = /^feed([0-9]+)/.exec($(this).attr('id'))[1];
        var jqxhr = $.ajax(`/post/`, {
            method: "POST",
            data:{"id":id},
            dataType: 'json'
        }).done(function (data) {
            post_done(data);
        }).fail(function (xhr, status) {
            post_fail(xhr, status);
        }).always(function () {
            closediv();//ajaxLog('请求完成: 无论成功或失败都会调用');
        });
    });
    feed.mousemove(function(e) {
        $(this).css('background-color', 'rgb(246,246,246)');
    });
    feed.mouseleave(function(e) {
        $(this).css('background-color', 'rgb(238,238,238)');
    });
});

function subscribe_done(data){
    closediv();
    if(data["status"] === "right"){
        alert(`订阅 "${data['title']}" 成功!`);
    }else if (data['status'] === "exist"){        
        alert(`"${data['title']}"已经订阅`);
    }else{
        alert(`订阅不存在`);
    }
}

function subscribe_fail(xhr, status){
    closediv();
    alert("网络错误");
}

$(function(){
    var sub = $('#sub')
    var sublink = $('#sublink')
    sub.click(function(){
        link = sublink.val();
        if(link){
            var jqxhr = $.ajax(`/subscribe/`, {
                method: "POST",
                data:{"link":link},
                dataType: 'json'
            }).done(function (data) {
                subscribe_done(data);
            }).fail(function (xhr, status) {
                subscribe_fail(xhr, status);
            }).always(function () {
                //ajaxLog('请求完成: 无论成功或失败都会调用');
                
            });
            showdiv("正在操作...")
        }else{
            alert("请输入订阅地址")
        }
        
    });
});

$(function (){
    $.ajaxSetup({headers: { "X-CSRFToken": getCookie("csrftoken")}});
});
function getCookie(name){
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
    if(arr=document.cookie.match(reg))
        return unescape(arr[2]);     
    else         
        return null; 
} 


var ws = new WebSocket("ws://localhost:8080/websocket");

ws.onopen = function() {
};

ws.onmessage = function (evt) {
    var warnpanel = $("#warnpanel");
    var warninfo = $("#warninfo");
    warnpanel.css("display", "block");
    json_data = JSON.parse(evt.data);
    warninfo.text(`更新提醒：${json_data.title}有${json_data.count}个更新`);
    update=$(`#update${json_data.feed_id}`);
    update.text(`${json_data.count}`);
};



function closeinfo(){
    var warnpanel = $("#warnpanel");
    var warninfo = $("#warninfo");
    warnpanel.css("display", "None");
    warninfo.text('');
}


//关闭等待窗口  
function closediv() {  
    //Close Div   
    document.body.removeChild(document.getElementById("bgDiv"));  
    document.getElementById("msgDiv").removeChild(document.getElementById("msgTitle"));  
    document.body.removeChild(document.getElementById("msgDiv"));  
}  
//显示等待窗口  
function showdiv(str) {  
    var msgw, msgh, bordercolor;  
    msgw = 400; //提示窗口的宽度   
    msgh = 100; //提示窗口的高度   
    bordercolor = "#336699"; //提示窗口的边框颜色   
    titlecolor = "#99CCFF"; //提示窗口的标题颜色   
  
    var sWidth, sHeight;  
    sWidth = window.screen.availWidth;  
    sHeight = window.screen.availHeight;  
  
    var bgObj = document.createElement("div");  
    bgObj.setAttribute('id', 'bgDiv');  
    bgObj.style.position = "absolute";  
    bgObj.style.top = "0";  
    bgObj.style.background = "#777";  
    bgObj.style.filter = "progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=25,finishOpacity=75";  
    bgObj.style.opacity = "0.6";  
    bgObj.style.left = "0";  
    bgObj.style.width = sWidth + "px";  
    bgObj.style.height = sHeight + "px";  
    document.body.appendChild(bgObj);  
    var msgObj = document.createElement("div")  
    msgObj.setAttribute("id", "msgDiv");  
    msgObj.setAttribute("align", "center");  
    msgObj.style.position = "absolute";  
    msgObj.style.background = "white";  
    msgObj.style.font = "12px/1.6em Verdana, Geneva, Arial, Helvetica, sans-serif";  
    msgObj.style.border = "1px solid " + bordercolor;  
    msgObj.style.width = msgw + "px";  
    msgObj.style.height = msgh + "px";  
    msgObj.style.top = (document.documentElement.scrollTop + (sHeight - msgh) / 2) + "px";  
    msgObj.style.left = (sWidth - msgw) / 2 + "px";  
    var title = document.createElement("h4");  
    title.setAttribute("id", "msgTitle");  
    title.setAttribute("align", "right");  
    title.style.margin = "0";  
    title.style.padding = "3px";  
    title.style.background = bordercolor;  
    title.style.filter = "progid:DXImageTransform.Microsoft.Alpha(startX=20, startY=20, finishX=100, finishY=100,style=1,opacity=75,finishOpacity=100);";  
    title.style.opacity = "0.75";  
    title.style.border = "1px solid " + bordercolor;  
    title.style.height = "18px";  
    title.style.font = "12px Verdana, Geneva, Arial, Helvetica, sans-serif";  
    title.style.color = "white";  
    //title.style.cursor = "pointer";  
    //title.innerHTML = "关闭";  
    //title.onclick = closediv;  
    document.body.appendChild(msgObj);  
    document.getElementById("msgDiv").appendChild(title);  
    var txt = document.createElement("p");  
    txt.style.margin = "1em 0"  
    txt.setAttribute("id", "msgTxt");  
    txt.innerHTML = str;  
    document.getElementById("msgDiv").appendChild(txt);  
}  
//屏蔽F5  
document.onkeydown = mykeydown;  
function mykeydown() {  
    if (event.keyCode == 116) //屏蔽F5刷新键     
    {  
        window.event.keyCode = 0;  
        return false;  
    }  
}