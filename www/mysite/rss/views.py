from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from .models import Feedjack_feed, Feedjack_post
import simplejson
import datetime
import feedparser
import xml
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

@csrf_exempt
def index(request):
    template = loader.get_template('rss/index.html')
    feedlist = Feedjack_feed.objects.all().filter(is_active=True)
    context = {
        "feedlist":feedlist,
    }
    return HttpResponse(template.render(context, request))


@csrf_exempt    
def get_post(request):
    feed_id = request.POST['id']
    feed_data = get_object_or_404(Feedjack_feed, pk=feed_id)
    post_list = []
    for metadata in feed_data.feedjack_post_set.all().order_by('-published_date'):
        ret = {}
        ret['id'] = metadata.id
        ret['title'] = metadata.title
        ret['date'] = metadata.published_date.strftime("%Y-%m-%d %H:%M:%S")
        if len(metadata.feedjack_post_class_set.all()) == 0:
            sentiment=None
        else:
            sentiment = metadata.feedjack_post_class_set.all()[0].post_sentiment
        ret['sentiment'] = sentiment
        post_list.append(ret)
    json_data = simplejson.dumps(post_list)
    return HttpResponse(json_data, content_type='application/json')
    
@csrf_exempt    
def get_content(request, post_id):
    post_data = get_object_or_404(Feedjack_post, pk=post_id)
    ret_data = {}
    ret_data['link'] = post_data.link
    ret_data['content'] = post_data.content
    json_data = simplejson.dumps(ret_data)
    return HttpResponse(json_data, content_type='application/json') 
    
@csrf_exempt    
def subscribe(request):
    feed_url = request.POST['link']
    d = feedparser.parse(feed_url)
    ret = dict()
    if hasattr(d, "status") and d.status >= 400 or hasattr(d, "bozo") and d.bozo==1 and isinstance(d.bozo_exception, xml.sax._exceptions.SAXParseException):
        ret['status'] = "wrong"
        ret['title'] = None
    elif len(Feedjack_feed.objects.filter(feed_url = feed_url)) != 0:
        ret['status'] = "exist"
        ret['title'] = d.feed.title
    else:
        ret['status'] = "right"
        ret['title'] = d.feed.title
        newfeed = Feedjack_feed(feed_url=feed_url)  
        newfeed.save()      
    json_data = simplejson.dumps(ret)
    return HttpResponse(json_data, content_type='application/json')    