# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-20 13:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Feedjack_feed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200)),
                ('feed_url', models.CharField(max_length=200)),
                ('title', models.CharField(blank=True, max_length=200)),
                ('etag', models.CharField(blank=True, max_length=50)),
                ('link', models.CharField(blank=True, max_length=200)),
                ('last_modified', models.DateTimeField(blank=True, null=True, verbose_name='last modified date')),
                ('last_checked', models.DateTimeField(blank=True, null=True, verbose_name='last checked date')),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Feedjack_post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=200)),
                ('author', models.CharField(blank=True, max_length=20)),
                ('link', models.CharField(blank=True, max_length=200)),
                ('content', models.TextField(blank=True)),
                ('guid', models.CharField(max_length=200)),
                ('published_date', models.DateTimeField(blank=True, null=True, verbose_name='published date')),
                ('update_date', models.DateTimeField(blank=True, null=True, verbose_name='update date')),
                ('feed', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rss.Feedjack_feed')),
            ],
        ),
        migrations.CreateModel(
            name='Feedjack_post_tags',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('feed_post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rss.Feedjack_post')),
            ],
        ),
        migrations.CreateModel(
            name='Feedjack_tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='feedjack_post_tags',
            name='feed_tags',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rss.Feedjack_tag'),
        ),
    ]
