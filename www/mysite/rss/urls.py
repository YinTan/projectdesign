from django.conf.urls import url
from . import views


app_name = 'rss'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^post/$', views.get_post, name='get_post'),
    url(r'^epost/(?P<post_id>[0-9]+)/$', views.get_content, name='get_content'),
    url(r'^subscribe/$', views.subscribe, name='subscribe'),
]

