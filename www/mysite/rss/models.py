from __future__ import unicode_literals
from django.db import models

# Create your models here.

class Feedjack_feed(models.Model):
    name = models.CharField(max_length = 200, blank=True)
    feed_url = models.CharField(max_length = 200)
    title = models.CharField(max_length=200, blank=True)
    etag = models.CharField(max_length=50, blank=True)
    link = models.CharField(max_length=200, blank=True)
    last_modified = models.DateTimeField('last modified date', null=True, blank=True)
    last_checked = models.DateTimeField('last checked date', null=True, blank=True)
    is_active = models.BooleanField(default=True)
    

class Feedjack_post(models.Model):
    feed = models.ForeignKey(Feedjack_feed, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, blank=True)
    author = models.CharField(max_length=100, blank=True)
    link = models.CharField(max_length=200, blank=True)
    content = models.TextField(blank=True)
    guid = models.CharField(max_length=200)
    published_date = models.DateTimeField('published date', null=True, blank=True)
    update_date = models.DateTimeField('update date', null=True, blank=True)
    
    

class Feedjack_tag(models.Model):
    name = models.CharField(max_length=50)
    
    
class Feedjack_post_tags(models.Model):
    feed_post = models.ForeignKey(Feedjack_post, on_delete=models.CASCADE)
    feed_tags = models.ForeignKey(Feedjack_tag, on_delete=models.CASCADE)

class Feedjack_post_class(models.Model):
    SENTIMENT = (
        ("P", "positive"),
        ("N", "negative"),
        ("M", "neutral"),
    )
    feed_post =  models.ForeignKey(Feedjack_post, on_delete=models.CASCADE)
    post_sentiment = models.CharField(max_length=1, choices=SENTIMENT, default='M')
    
 
