# -*- coding: utf-8 -*-
import os
import sys
import re
import socket
from random import uniform
import time
from datetime import datetime, timedelta
from dateutil import tz
import psycopg2
import psycopg2.extras
from pprint import pformat, pprint
import pickle
from bs4 import BeautifulSoup
import jieba
import jieba.analyse
#custom module
import settings

__author__ = 'gli'






def encode(tstr):
    """ Encodes a unicode string in utf-8
    """
    if not tstr:
        return ''
    # this is _not_ pretty, but it works
    try:
        return tstr.encode('utf-8', "xmlcharrefreplace")
    except UnicodeDecodeError:
        # it's already UTF8.. sigh
        return tstr.decode('utf-8').encode('utf-8')


def prints(tstr):
    """ lovely unicode
    """
    sys.stdout.write('%s\n' % (tstr.encode(sys.getdefaultencoding(), 'replace')))
    sys.stdout.flush()


def mtime(time):
    """ Datetime auxiliar function.
    """
    # http://stackoverflow.com/questions/4770297/python-convert-utc-datetime-string-to-local-datetime
    # Auto-detect zones:
    from_zone = tz.tzutc()
    to_zone = tz.tzlocal()
    utc = datetime.strptime(time, "%a, %d %b %Y %H:%M:%S GMT")
    # Tell the datetime object that it's in UTC time zone since
    # datetime objects are 'naive' by default
    utc = utc.replace(tzinfo=from_zone)
    # Convert time zone
    central = utc.astimezone(to_zone)
    return central


def logger(message):
    print "%s: %s [%s]" % (datetime.now(), message, sys.argv[0])


def logger_count(count):
    logger("Processed %s" % count)
    
    
def get_pure_text_from_html(htmlstr):
    soup = BeautifulSoup(htmlstr, 'lxml')
    return soup.get_text()
    
def get_tf_idf(text):
    return jieba.analyse.extract_tags(sentence, topK=20, withWeight=True)

def convertline(line, word2id):
    ret = dict()
    for word in jieba.analyse.extract_tags(line, topK=20, withWeight=True):
        wordid = word2id.get_id(word[0])
        ret[wordid] = word[1]
    return ret    
        
class Word2id(object):
    def __init__(self, wordfile="./trainfile/wordfile", idfile="./trainfile/idfile"):
        self.wordict = None
        self.idict = None
        self.wordfile = wordfile
        self.idfile = idfile
        self.isload = False
        try:
            with open(self.wordfile, "r") as f, open(self.idfile, "r") as t:
                self.wordict = pickle.load(f)
                self.idict = pickle.load(t)
                self.isload = True
        except:
            pass
        if not self.isload:
            self.wordict = dict()
            self.idict = dict()
        self.maxid = len(self.wordict)
        self.needsave = False
    
    def __len__(self):
        return self.maxid
            
    def get_id(self, word):
        if word in self.wordict:
            return self.wordict.get(word)
        else:
            self.maxid += 1
            self.needsave = True
            self.wordict[word] = self.maxid
            self.idict[self.maxid] = word
            return self.maxid
    
    def get_word(self, id):
        return self.idict.get(id, None)
    
    def save(self):
        if self.needsave:
            with open(self.wordfile,"wb") as f, open(self.idfile,"wb") as t:
                pickle.dump(self.wordict, f)
                pickle.dump(self.idict, t)

