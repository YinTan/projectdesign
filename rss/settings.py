


'''
for feedparser
'''
VERSION = '0.0.0'
URL = 'http://www.example.com'
USER_AGENT = 'rssfeed %s - %s' % (VERSION, URL)


# Program's general settings
DEBUG = False
# Should I be verbose?
DEFAULT_VERBOSE = False
# Suspend time in seconds when connecting to feeds
DEFAULT_SLEEPTIME = 30
# Wait timeout in seconds when connecting to feeds
DEFAULT_SOCKETTIMEOUT = 10
# Number of worker threads that will fetch feeds in parallel
DEFAULT_WORKERTHREADS = 10
# This is a slow feed if the downloading time exceeds this limit
SLOWFEED_WARNING = 10
# Log format
LOG_FORMAT = '%(asctime)s - %(levelname)s: %(name)s: %(message)s'


# Postgresql database settings
pg_db_host = "localhost"
pg_db_port = 5432
pg_db_dbname = "rssdata"
pg_db_username = "postgres"
pg_db_password = "postgres"
pg_db_dsn = "host=%(host)s port=%(port)s dbname=%(dbname)s user=%(username)s password=%(password)s" % {
    "host": pg_db_host,
    "port": pg_db_port,
    "dbname": pg_db_dbname,
    "username": pg_db_username,
    "password": pg_db_password,
    }

