# -*- coding: utf-8 -*-

import logging
import sys
import psycopg2
import psycopg2.extras
from datetime import datetime, timedelta
import time
try:
    import tracebackturbo as traceback
except:
    import traceback
import settings
#custom
from misc import *

ENTRY_NEW, ENTRY_UPDATED, ENTRY_SAME, ENTRY_ERR = range(4)

logger = logging.getLogger(__name__)

db_conn = None


def connect_pg():
    global db_conn
    try:
        db_conn = psycopg2.connect(settings.pg_db_dsn)
    except:
        logger.info(sys.exc_info())
        quit()

#主要的entry的逻辑处理程序
class ProcessEntry(object):
    def __init__(self, feed, entry, post_dict):
        self.feed = feed
        self.entry = entry
        self.post_dict = post_dict
    
    def get_tags(self):
        ret_tags = []
        if 'tags' in self.entry:
            for tag in self.entry.tags:
                if tag['label'] != None:
                    term = tag['label']
                elif tag['term'] != None:
                    term = tag['term']
                try:
                    if db_conn is None:
                        connect_pg()
                    cursor = db_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
                    cursor.execute("SELECT id FROM rss_feedjack_tag "\
                                   "WHERE name=%s",
                                   (term,))
                    results = cursor.fetchall()
                    if results:
                        ret_tags.append({'id':results[0]['id'],
                                         'name':term})
                    else:
                        cursor.execute("INSERT INTO rss_feedjack_tag (name) "\
                                       "VALUES (%s)",
                                       (term,))
                        db_conn.commit()
                        cursor.execute("SELECT id FROM rss_feedjack_tag "\
                                       "WHERE name=%s",
                                       (term,))
                        results = cursor.fetchall()
                        if results:
                            ret_tags.append({'id':results[0]['id'],
                                             'name':term})
                except:
                    db_conn.rollback()
                    logger.info('process tags failed')
                    (etype, eobj, etb) = sys.exc_info()
                    traceback.print_exception(etype, eobj, etb)
        return ret_tags



    def get_entry_data(self):
        
        link = self.entry.get('link', self.feed['link'])[0:200]
        title = self.entry.get('title', link)[0:200]    
        try:
            content = self.entry['content'][0]['value']
        except:
            content = self.entry.get('summary', '')            
        author = self.entry.get('author', 'noname')[0:100]
        if hasattr(self.entry, 'published_parsed') and (self.entry['published_parsed'] is not None):
            published_date = datetime.strptime(time.strftime("%Y-%m-%d %H:%M:%S", self.entry['published_parsed']), '%Y-%m-%d %H:%M:%S')
        else:
            published_date = datetime.now()
        if hasattr(self.entry, 'update_parsed') and (self.entry['update_parsed'] is not None):
            update_date = datetime.strptime(time.strftime("%Y-%m-%d %H:%M:%S", self.entry['update_parsed']), '%Y-%m-%d %H:%M:%S')
        else:
            update_date = datetime.now()
        tags = self.get_tags()
        guid = (link+title+str(len(content))+author)[0:199]
        return (link, title, content, author, guid, published_date, update_date, tags)
        
    
    def process(self):
        (link, title, content, author, guid, 
         published_date, update_date, tags) = self.get_entry_data()
        post_id = None
        retval = None
        logger.debug(u'Entry\n'\
                     u'  title: %s\n'\
                     u'  link: %s\n'\
                     u'  guid: %s\n'\
                     u'  author: %s\n'\
                     u'  tags: %s' % (
                     title, link, guid, author,
                     u' '.join(tag['name']for tag in tags)
                     ))     
        if guid.encode('utf8') in self.post_dict:
            #logger.info('[guid %s] [title %s] in post_dict' % (guid, title))
            retval = ENTRY_SAME
        #如果之前的信息里面没有
        else:
            retval = ENTRY_NEW
            logger.info('[Feed id: %d] Saving new post: %s' % (self.feed['id'], link))
            global db_conn
            try:
                if db_conn is None:
                    connect_pg()
                #logger.info('insert new post to db...')    
                cursor = db_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
                cursor.execute("INSERT INTO rss_feedjack_post "\
                               "(feed_id, link, title, content, author, "\
                               "guid, published_date, update_date) "\
                               "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                               (self.feed['id'], link, title, content, author, \
                                guid, published_date, update_date))                                
                db_conn.commit()
                #logger.info('insert new post completed...')
                
                #logger.info('re_select post_id from db...')                
                cursor.execute("SELECT id FROM rss_feedjack_post WHERE "\
                               "feed_id=%s AND guid=%s",
                               (self.feed['id'], guid))
                #logger.info('re_select post_id completed...')
                
                results = cursor.fetchall()
                if results:
                    post_id = results[0]['id']
                
                #process tags
                if tags != None:
                    for tag in tags:
                        tag_id = tag['id']
                        tag_name = tag['name']
                        if post_id and tag_id:
                            cursor.execute("INSERT INTO rss_feedjack_post_tags "\
                                           "(feed_post_id, feed_tags_id) VALUES (%s, %s)",
                                           (post_id, tag_id))
                            db_conn.commit()
            except:
                db_conn.rollback()
                (etype, eobj, etb) = sys.exc_info()
                traceback.print_exception(etype, eobj, etb)
            
        post = {'post_id': post_id,
                'feed_id': self.feed['id'],
                'feed_title':self.feed['title'], 
                'title': title,
                'link': link, 
                'content': content,
                'guid':guid,
                'author': author,
                'published_date': published_date
                }
        return retval, post
            
class ProcessFeed(object):

    def __init__(self):
        self.feed = {}
        
    def process(self, data):
        logger.info('Processing feed data with feed_id: %d' % (data['feed_id'],))
        
        self.feed['id'] = data['feed_id']
        self.feed['etag'] = data.feed.get('etag', '')
        
        try:
            self.feed['last_modified'] = data.feed['published_parsed']            
        except:
            self.feed['last_modified'] = data.feed.get('update_parsed', None)
        if self.feed['last_modified'] is None:
            self.feed['last_modified'] = datetime.now()
        else:
            self.feed['last_modified'] = datetime.strptime(
                                              time.strftime("%Y-%m-%d %H:%M:%S", self.feed['last_modified']),
                                              '%Y-%m-%d %H:%M:%S')    
        self.feed['link'] = data['feed'].get('link', '')
        self.feed['title'] = data['feed'].get('title', self.feed['link'])[0:254]
        self.feed['last_checked'] = datetime.now()
        
        logger.info('Updating "rss_feedjack_feed" table in db')
        logger.info('title=%s \n'\
                    'etag=%s  \n' % (self.feed['title'], self.feed['etag']))
        try:
            if db_conn is None:
                connect_pg()            
            cursor = db_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            
            cursor.execute("UPDATE rss_feedjack_feed SET " \
                           "title=%s, link=%s, etag=%s, " \
                           "last_modified=%s, last_checked=%s " \
                           "WHERE id=%s",
                           (self.feed['title'], self.feed['link'], 
                            self.feed['etag'], self.feed['last_modified'], 
                            self.feed['last_checked'], self.feed['id']))
            db_conn.commit()
        except:
            db_conn.rollback()
            (etype, eobj, etb) = sys.exc_info()
            traceback.print_exception(etype, eobj, etb)        
        logger.info('Updating "feedjack_feed" table in database completed')
        
        existing_posts = set()
        try:
            if db_conn is None:
                connect_pg()
            
            cursor = db_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cursor.execute("SELECT guid "\
                           "FROM rss_feedjack_post " \
                           "WHERE feed_id=%s", (self.feed['id'],))
            results = cursor.fetchall()
            
            if results is None or len(results) == 0:
                logger.info('existing_posts is empty...')
                
            for result in results:
                existing_posts.add(result['guid'])
        except:
            db_conn.rollback()
            (etype, eobj, etb) = sys.exc_info()
            traceback.print_exception(etype, eobj, etb)  
        entries_status = {ENTRY_NEW: 0,
                          ENTRY_UPDATED: 0,
                          ENTRY_SAME: 0,
                          ENTRY_ERR: 0}
        logger.info('existing_posts\'s length: %d'%(len(existing_posts),))
        new_post_list = []
        for entry in data['entries']:
            try:
                (entry_status, post) = self.process_entry(entry, existing_posts)
                #if entry_status and post:
                if entry_status == ENTRY_NEW:
                    new_post_list.append(post)
                    entries_status[entry_status] += 1
            except:
                (etype, eobj, etb) = sys.exc_info()
                print '[%d] ! -------------------------' % (self.feed['id'],)
                traceback.print_exception(etype, eobj, etb)
                print '[%d] ! -------------------------' % (self.feed['id'],)
                entry_status = ENTRY_ERR

        return entries_status, new_post_list    

    def process_entry(self, entry, existing_posts):
        pentry = ProcessEntry(self.feed, entry, existing_posts)
        (entry_status, post) = pentry.process()
        del pentry
        return entry_status, post




