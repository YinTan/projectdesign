from threading import Thread
import sys
import signal
import time
    
    
class test(Thread):
    
    def __init__(self):
        super(test, self).__init__()
        
        
    def run(self):
        num = 100
        print('slave start')
        for i in range(10, 0, -1):
            print('Num: {0}'.format(num/i))
            time.sleep(1)
        print('slave end')


class myiter(object):
    def __init__(self):
        self.i = 0
    def next(self):
        if self.i < 10:
            self.i+=1
            return self.i
        else:
            self.i = 0
            raise StopIteration()
    def __iter__(self):
        return self
        
                       
if __name__ == '__main__':
    # print('main start')
    # t = test()
    # t.setDaemon(True)
    # t.start()
    # try:
    #     while t.isAlive():
    #         pass
    # except:
    #     print('thread stopped by keyboard')
    # print('main end')
    m = myiter()
    for i in range(10):
        for a in m:
            print a
       