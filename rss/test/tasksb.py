# -*- coding: utf-8 -*-
from celery import Celery

appb = Celery('tasksb', backend='amqp://', broker='amqp://')
#app.config_from_object('config')
@appb.task(name="multi")
def multi(x):
    print x*3
    return 3 * x
