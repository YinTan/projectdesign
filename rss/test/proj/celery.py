# -*- coding: utf-8 -*-

from __future__ import absolute_import
from celery import Celery


app = Celery('proj', broker='amqp://', worker='amqp://', include=['proj.tasks','proj.calculate'])
app.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
)
