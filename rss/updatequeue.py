# -*- coding: utf-8 -*-

import pika
import json


class UpdatePost(object):
    EXCHANGE = 'message'
    EXCHANGE_TYPE = 'topic'
    QUEUE = 'text'
    ROUTING_KEY = 'newpost.text'
    AMQP_URL = 'amqp://guest:guest@localhost:5672/%2F'
    
    def __init__(self):
        self._connection = pika.BlockingConnection(parameters = pika.URLParameters(self.AMQP_URL))
        self._channel = self._connection.channel()
            
    def publish_message(self, message):
        properties = pika.BasicProperties(content_type='application/json',
                                           delivery_mode=1)
        self._channel.basic_publish(self.EXCHANGE, self.ROUTING_KEY, json.dumps(message, ensure_ascii=False), properties)
        
        
class Textparse(object):
    EXCHANGE = 'message'
    EXCHANGE_TYPE = 'topic'
    QUEUE = 'parser'
    ROUTING_KEY = 'textparse.parser'
    AMQP_URL = 'amqp://guest:guest@localhost:5672/%2F'
    def __init__(self):
        self._connection = pika.BlockingConnection(parameters = pika.URLParameters(self.AMQP_URL))
        self._channel = self._connection.channel()
            
    def publish_message(self, message):
        properties = pika.BasicProperties(content_type='application/json',
                                           delivery_mode=1)
        self._channel.basic_publish(self.EXCHANGE, self.ROUTING_KEY, json.dumps(message, ensure_ascii=False), properties)
        