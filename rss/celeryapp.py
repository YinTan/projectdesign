# -*- coding: utf-8 -*-
from celery import Celery

app = Celery('rss',worker="amqp://", include=["rss.parseworker", "rss.maggotworker"])
#app = Celery('rss',worker="amqp://", include=["rss.maggotworker"])
#app = Celery('rss',worker="amqp://", include=["rss.parseworker"])
app.conf.beat_schedule = {
    'add-every-7200-seconds': {
        'task': 'rss.parseworker.updateandparse',
        'schedule': 2,
        'args': None
    },
}
app.conf.timezone = 'UTC'
app.conf.task_serializer = 'pickle'
app.conf.accept_content = ['pickle']