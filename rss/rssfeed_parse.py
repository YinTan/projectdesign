
# -*- coding: utf-8 -*-
import sys
import logging
import optparse
import feedparser
from datetime import datetime, timedelta
#use postgresql
import psycopg2
import psycopg2.extras
import xml
try:
    import tracebackturbo as traceback
except:
    import traceback
from maggotworker import maggot
#this is the custom module
import settings

FEED_OK, FEED_UNCHANGED, FEED_ERRPARSE, FEED_ERRHTTP, FEED_ERREXC = range(5)

logger = logging.getLogger(__name__)

#keep None for testing
db_conn = None

#connect to DB
def connect_db():
    global db_conn
    try:
        db_conn = psycopg2.connect(settings.pg_db_dsn)
    except:
        logger.info(sys.exc_info())
        quit()


class Dispatcher(object):
    
    def __init__(self):
        self.feed_status_dict = {FEED_OK: 0,
                                 FEED_UNCHANGED: 0,
                                 FEED_ERRPARSE: 0,
                                 FEED_ERRHTTP: 0,
                                 FEED_ERREXC: 0}
        self.feed_trans = {FEED_OK: 'OK',
                           FEED_UNCHANGED: 'UNCHANGED',
                           FEED_ERRPARSE: 'CAN\'T PARSE',
                           FEED_ERREXC: 'EXCEPTION'}
        self.feed_keys = sorted(self.feed_trans.keys())
        self.time_start = datetime.now()
        self.feeds = []
        self._update_feeds()
        
    def add_job(self, feed):
        '''
        1.feed?
        adds a feed processing job to the pool
        '''
        logger.info(u'Processing feed [id:%d], [feed_url: %s], [feed_etag: %s]' % (feed['id'], feed['feed_url'], feed['etag']))
        start_time = datetime.now()
        data, feed_status = self._process_feed(feed)
        
        delta = datetime.now() - start_time
        self.feed_status_dict[feed_status] += 1
        return data

    def _process_feed(self, feed):
        '''
        it checks the Etag and the last-modified time so the feed include at least {Etag, url, time, id, ?}
        status(304) means the feed has not changed.
        '''

        try:
            data = feedparser.parse(feed['feed_url'],
                                    etag=feed['etag'])
        except:
            logger.info('ERROR: feed cannot be parsed')
            (etype, eobj, etb) = sys.exc_info()
            traceback.print_exception(etype, eobj, etb)
            return None, FEED_ERRPARSE
        try:
            if hasattr(data, 'status'):
                logger.info(u'HTTP status: %d' % (data.status,))
                if data.status == 304:
                    logger.info('Feed [id:%d] has not changed since last check' % (feed['id'],))
                    feed_status = FEED_UNCHANGED
                elif data.status >= 400:
                    logger.info('Feed [id:%d] HTTP Error' % (feed['id'],))
                    feed_status = FEED_ERRHTTP
                else:
                    feed['etag'] = data.get('etag', '')
                    # feed['last_modified'] = data.get('last_modified', data.get('modified', None))
                    feed_status = FEED_OK
            if hasattr(data, 'bozo') and \
                           data.bozo and \
                           isinstance(data.bozo_exception, 
                                        xml.sax._exceptions.SAXParseException):
                feed_status = FEED_ERRHTTP
                logger.info('BOZO! Feed [id:%d] is not well formed' % (feed['id'],))
        except:
            (etype, eobj, etb) = sys.exc_info()
            print '[%d] ! ---------------------------' % (feed['id'],)
            traceback.print_exception(etype, eobj, etb)
            print '[%d] ! ---------------------------' % (feed['id'],)
            feed_status = FEED_ERREXC
        if feed_status == FEED_OK:
            return data, feed_status
        else:
            return None, feed_status
            
            
    def _update_feeds(self):
        global db_conn
        try:
            logger.info('Obtaining feeds to process from database')
            if db_conn is None:
                logger.info('Connect to database')
                connect_db()
            cursor = db_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cursor.execute("SELECT id, name, feed_url, etag "\
                           "FROM rss_feedjack_feed "\
                           "WHERE is_active=%s "\
                           "ORDER BY id", (True,))
            results = cursor.fetchall()
            for result in results:
                self.feeds.append({'id': result['id'],
                                  'name': result['name'],
                                  'feed_url': str(result['feed_url']),
                                  'etag': str(result['etag'])})
                                  
            logger.info("Obtaining feeds to process from database completed")
        except:
            db_conn.rollback()
            (etype, eobj, etb) = sys.exc_info()
            traceback.print_exception(etype, eobj, etb)


class RssFeedBee(object):
    def __init__(self):
        self.dispatcher = Dispatcher()
        
    def process(self):
            for feed in self.dispatcher.feeds:
                try:
                    data = self.dispatcher.add_job(feed)
                except KeyboardInterrupt:
                    break
                    print 'quit!!'
                except:
                    logger.info('parse feed[feed_id:%d] failed' % feed['id'])
                    continue
                if data is not None and isinstance(data, dict):
                    data['feed_id'] = feed['id']
                    logger.info(data.feed.get('title', 'no title'))
                    logger.info('send data to queue...')
                    maggot.delay(data)
        

        
def main():
    logging.basicConfig(level=logging.INFO)
    rss = RssFeedBee()
    rss.process()
    
        
if __name__ == '__main__':
    main()            


