
from celeryapp import app
import rssfeed_maggot
from updatequeue import UpdatePost
from updatequeue import Textparse

proc_feed = rssfeed_maggot.ProcessFeed()

@app.task
def maggot(data):
    (entries_status, post_list) = proc_feed.process(data)
    if(entries_status[0] > 0):
        updatepost = UpdatePost()
        parsepost = Textparse()
        updateinfo = {}
        updateinfo['feed_id'] = post_list[0]['feed_id']
        updateinfo['title'] = post_list[0]['feed_title']
        updateinfo['count'] = entries_status[0]
        updatepost.publish_message(updateinfo)
        for post in post_list:
            classdata = {}
            classdata['post_id'] = post['post_id']
            classdata['content'] = post['content']
            parsepost.publish_message(classdata)