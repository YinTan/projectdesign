# -*- coding: utf-8 -*-
import jieba.analyse
try:
    import cPickle as pickle
except ImportError:
    import pickle
import feedparser
from svm.svm import *
from svm.svmutil import *
from misc import Word2id
from misc import get_pure_text_from_html
from misc import convertline
#1好事  0坏事 2中
#改为 1好事 0中 -1坏事                

                
class Train(object):
    def __init__(self, filepath="./trainfile/train", testpath="./trainfile/train", test=False):
        self.word2id = None
        self.filepath = filepath
        self.testpath = testpath
        self.init = False
        self.test = test
        self.label = list()
        self.data = list()
        self.testlabel = list()
        self.testdata = list()
    def initialize(self):
        self.word2id = Word2id()  
        self.construct_wordset()
        print "count words %d"%(len(self.word2id),)
        if self.test:
            self.construct_testset()
        self.init = True
                
    def construct_wordset(self):
        try:
            f = open(self.filepath, "r")
        except:
            print "open file %s failed..." %(self.filepath,)
            return None
        while True:
            line1 = f.readline()
            if not line1:
                break
            line2 = f.readline()
            typeid = int(line1)
            datalist = convertline(line2, self.word2id)
            self.label.append(typeid)
            self.data.append(datalist)
    
    def construct_testset(self):
        try:
            f = open(self.testpath, "r")
        except:
            print "open file %s failed..." %(self.testpath,)
            return None
        while True:
            line1 = f.readline()
            if not line1:
                break
            line2 = f.readline()
            type(line2)
            typeid = int(line1)
            datalist = convertline(line2, self.word2id)
            self.testlabel.append(typeid)
            self.testdata.append(datalist) 
            
               
    def train(self):
        if not self.init:
            self.initialize()
        prob = svm_problem(self.label, self.data)
        param = svm_parameter('-t 0')
        m = svm_train(prob, param)
        if self.test:
            p_labels, p_acc, p_vals = svm_predict(self.testlabel, self.testdata, m)
            # for i in range(len(p_labels)):
            #     print "predict: ", p_labels[i]
            #     print "truth: ", p_labels[i]
            #     print "key words"
            #     for key in self.testdata[i]:
            #         print "key",key
            #         print self.word2id.get_word(key) 
        svm_save_model('./trainfile/predict.model', m)
        #m = svm_load_model('heart_scale.model')
        self.word2id.save()
        if self.test:
            return p_labels
    
                       

        
def construct_trainfile():
    feed_list = ["http://www.xinhuanet.com/politics/news_politics.xml", 
                 "http://www.xinhuanet.com/world/news_world.xml",
                 "http://www.xinhuanet.com/health/news_health.xml",
                 "http://finance.ifeng.com/rss/",
                 "http://www.ftchinese.com/rss/news",
                 "feed://news.qq.com/newsgn/rss_newsgn.xml",
                 "feed://news.qq.com/newssh/rss_newssh.xml",
                 "feed://finance.qq.com/financenews/breaknews/rss_finance.xml",
                 "feed://rss.huanqiu.com/china/local.xml",
                 "feed://rss.huanqiu.com/china/roll.xml"]
                  
    test_list = ["feed://news.qq.com/nehemu/rss_nmhm.xml",
                 "feed://rss.huanqiu.com/finance/data.xml",
                 "feed://rss.huanqiu.com/china/politics.xml"]
    index = 0;
    with open("./trainfile/train", "w") as f:
        for feed_url in feed_list:
            print "parse feed_url:%s" %(feed_url,)
            data = feedparser.parse(feed_url)
            if hasattr(data, "status") and data.status >= 400:
                continue
            for entry in data.entries:
                if index % 10 == 0:
                    print "get %d"%(index,)
                try:
                    content = entry['content'][0]['value']
                except:
                    content = entry.get('summary', '')
                f.write(get_pure_text_from_html(content).encode("utf8"))
                f.write('\n')
                index+=1
    index = 0
    with open("./trainfile/test", "w") as f:
        for test_url in test_list:
            print "parse test_url:%s"%(test_url,)
            data = feedparser.parse(feed_url)
            if hasattr(data, "status") and data.status >= 400:
                continue
            for entry in data.entries:
                if index % 10 == 0:
                    print "get %d"%(index,)
                try:
                    content = entry['content'][0]['value']
                except:
                    content = entry.get('summary', '')
                f.write(get_pure_text_from_html(content).encode("utf8"))
                f.write("\n")
                index += 1
def statistic():
    with open("./trainfile/test", "r") as f:
        ret={0:0, 1:0, -1:0}
        while True:
            sent = f.readline()
            if not sent:
                break
            num = int(sent)
            ret[num]+=1
            f.readline() 
    for key in ret.keys():
        print key, ret[key]
        
def source():
    feed_list = ["http://www.xinhuanet.com/politics/news_politics.xml", 
                 "http://www.xinhuanet.com/world/news_world.xml",
                 "http://www.xinhuanet.com/health/news_health.xml",
                 "http://finance.ifeng.com/rss/",
                 "http://www.ftchinese.com/rss/news",
                 "feed://news.qq.com/newsgn/rss_newsgn.xml",
                 "feed://news.qq.com/newssh/rss_newssh.xml",
                 "feed://finance.qq.com/financenews/breaknews/rss_finance.xml",
                 "feed://rss.huanqiu.com/china/local.xml",
                 "feed://rss.huanqiu.com/china/roll.xml"]
                  
    test_list = ["feed://news.qq.com/nehemu/rss_nmhm.xml",
                 "feed://rss.huanqiu.com/finance/data.xml",
                 "feed://rss.huanqiu.com/china/politics.xml"]
    
    for url in test_list:
        data = feedparser.parse(url) 
        if hasattr(data, "status") and data.status >= 400:
            print url, " error"
            continue           
        print data.feed.get('title', 'no title')
        
def wholetest(p_labels):
    index=0
    evalist = list()
    ret={"0 -> 0":0, "0 -> 1":0, "0 -> -1":0, "1 -> 0":0, "1 -> 1":0, "1 -> -1":0,"-1 -> 0":0, "-1 -> 1":0, "-1 -> -1":0}
    with open("./trainfile/test", "r") as f:
        while True:
            sentiment=f.readline()
            if not sentiment:
                break
            num=int(sentiment)
            if num==0 and p_labels[index]==0:
                ret["0 -> 0"] += 1
            elif num==0 and p_labels[index]==1:
                ret["0 -> 1"] += 1
            elif num==0 and p_labels[index]==-1:
                ret["0 -> -1"] += 1
            elif num==1 and p_labels[index]==0:
                ret["1 -> 0"] += 1
            elif num==1 and p_labels[index]==1:
                ret["1 -> 1"] += 1
            elif num==1 and p_labels[index]==-1:
                ret["1 -> -1"] += 1
            elif num==-1 and p_labels[index]==0:
                ret["-1 -> 0"] += 1
            elif num==-1 and p_labels[index]==1:
                ret["-1 -> 1"] += 1
            elif num==-1 and p_labels[index]==-1:
                ret["-1 -> -1"] += 1 
            index+=1
            f.readline() 
            evalist.append(num) 
    for key in ret.keys():
        print key, ret[key]
    (ACC, MSE, SCC) = evaluations(evalist, p_labels)
    print "ACC:", ACC
    print "MSE", MSE
    print "SCC", SCC
    

if __name__ == '__main__':
    #构造数据
    #construct_trainfile()
    #训练数据
    t = Train(test=True)
    p = t.train()
    wholetest(p)
    #统计数据分分布
    #statistic()
    #统计数据的来源
    #source()